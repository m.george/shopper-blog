$('#search-field').ghostHunter({
    results: '#results',
    item_preprocessor: (item) => {
        var ret = {};
        // Add feature image to results
        ret.feature_image = item.feature_image;
        return ret;
    },
    result_template:
        '<a id="gh-{{ref}}" class="gh-search-item search-result" href="{{link}}"><img src="{{feature_image}}" /><h2>{{title}}</h2></a>',
    info_template:
        '<p class="search-info">Number of blogs found: {{amount}}</p>',
});
